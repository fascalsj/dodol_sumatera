/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekspresi_wajah;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

/**
 *
 * @author fascalCbn
 */
public class fungsi
{

    static int grayscale(int R, int G, int B)
    {
        int Grey = (int) (0.299 * R + 0.587 * G + 0.114 * B);

        return Grey;

    }
    static int binary(int R, int G, int B)
    {
        int Grey = (int) (0.299 * R + 0.587 * G + 0.114 * B);
        int tr = 113;
        if(Grey > 128 )
        {
            tr = 255;
        }
        else
        {
            tr = 0;
        }

        return tr;

    }

    static int HSV(int R, int G, int B)
    {

        int V = (int) Math.max(B, Math.max(R, G));
        int Vm = (int) Math.min(B, Math.min(R, G));

        int H = 0;
        if (V == 0)
        {
            int S = 0;
        } else if (V > 0)
        {
            int S = Vm / V;
        }
        if (R == V)
        {
            H = (60 / 360) * ((G - B / Vm));
        } else if (G == V)
        {
            H = (60 / 360) * (2 + ((B - R) / Vm));
        } else if (B == V)
        {
            H = (60 / 360) * (4 + ((R - G) / Vm));
        }

        return H;

    }
//    

    static int YCbCr(int R, int G, int B)
    {
        int Y = (int) (0.299 * R + 0.587 * G + 0.114 * B);
        int Cb = (int) (0.169 * R + 0.332 * G + 0.500 * B);
        int Cr = (int) (0.500 * R + 0.419 * G + 0.081 * B);
        int K;
        if (((Cb >= 77)&&(Cb <= 127))&&((Cr >= 90)&&(Cr <= 173))){
            K = 255;
        }
        else
        {
            K = 0;
        }
       

        return K;

    }

    static ImageIcon resizeGambar(BufferedImage image, int height, int weight)
    {
        ImageIcon imageAsli = new ImageIcon(image.getScaledInstance(128, 128, Image.SCALE_DEFAULT));

        return imageAsli;
    }

    static ImageIcon gambarAsli(BufferedImage image, int width, int height)
    {
        ImageIcon imageAsli = new ImageIcon(image.getScaledInstance(width, height, Image.SCALE_DEFAULT));

        return imageAsli;
    }

    static ImageIcon gambarGreyscale(BufferedImage image, int width, int height)
    {

        int[][] warnaGrey = new int[height][width];
        for (int i = 0; i < height; i++)
        {

            for (int j = 0; j < width; j++)
            {
                Color c = new Color(image.getRGB(j, i));
                int R = (int) (c.getRed());
                int G = (int) (c.getGreen());
                int B = (int) (c.getBlue());
                int Wrn = YCbCr(R, G, B);

                Color newColor = new Color(Wrn, Wrn, Wrn);
                warnaGrey[i][j] = R + G + B;
                image.setRGB(j, i, newColor.getRGB());
            }
        }
        ImageIcon imageGrayscale = new ImageIcon(image.getScaledInstance(width, height, Image.SCALE_DEFAULT));
        return imageGrayscale;
    }

    static ImageIcon gambarHSV(BufferedImage image, int width, int height)
    {

        int[][] warnaGrey = new int[height][width];
        for (int i = 0; i < height; i++)
        {

            for (int j = 0; j < width; j++)
            {
                Color c = new Color(image.getRGB(j, i));
                int R = (int) (c.getRed());
                int G = (int) (c.getGreen());
                int B = (int) (c.getBlue());
                int Wrn = YCbCr(R, G, B);
                int V = (int) Math.max(B, Math.max(R, G));
                int Vm = (int) Math.min(B, Math.min(R, G));

                int H = 0;
                if (V == 0)
                {
                    int S = 0;
                } else if (V > 0)
                {
                    int S = Vm / V;
                }
                if (R == V)
                {
                    H = (60 / 360) * ((G - B / Vm));
                } else if (G == V)
                {
                    H = (60 / 360) * (2 + ((B - R) / Vm));
                } else if (B == V)
                {
                    H = (60 / 360) * (4 + ((R - G) / Vm));
                }

                Color newColor = new Color(Wrn, Wrn, Wrn);
                warnaGrey[i][j] = R + G + B;
                image.setRGB(j, i, newColor.getRGB());
            }
        }
        ImageIcon imageGrayscale = new ImageIcon(image.getScaledInstance(width, height, Image.SCALE_DEFAULT));
        return imageGrayscale;
    }
}
